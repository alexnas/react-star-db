import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Header from '../header';
import RandomPlanet from '../random-planet';
import ErrorBoundry from '../error-boundry';
import SwapiService from '../../services/swapi-service';
import DummySwapiService from '../../services/dummy-swapi-service';
import { SwapiServiceProvider } from '../swapi-service-context';
import {
  PeoplePage,
  PlanetsPage,
  StarshipsPage,
  LoginPage,
  SecretPage
} from '../pages';

import './app.css';
import { StarshipDetails } from '../sw-components';


export default class App extends Component {

  state = {
    swapiService: new SwapiService()
  };

  onLogin = () => {
    this.setState({
      isLoggedIn: true
    })
  }

  onServiceChange = () => {
    this.setState(({ swapiService }) => {
      const Service = swapiService instanceof SwapiService ?
        DummySwapiService : SwapiService;
      return {
        swapiService: new Service(),
        isLoggedIn: false
      }
    })
  }

  render() {
    const { isLoggedIn } = this.state;
    return (
      <ErrorBoundry>
        <SwapiServiceProvider value={this.state.swapiService} >
          <Router>
            <div className="stardb-app">
              <Header onServiceChange={this.onServiceChange} />
              <RandomPlanet />

              <Switch>
                <Route path="/"
                  exact render={() => <h2>Welcome to StarDB!</h2>} />
                <Route path="/people/:id?" component={PeoplePage} />
                {/* <Route path="/people"
                  exact render={() => <h2>Welcome to StarDB!</h2>} /> */}
                <Route path="/planets" component={PlanetsPage} />
                <Route path="/starships" exact component={StarshipsPage} />
                <Route path="/starships/:id"
                  render={({ match, location, history }) => {
                    const { id } = match.params
                    return <StarshipDetails itemId={id} />
                  }} />
                <Route path="/login"
                  render={() => (
                    <LoginPage
                      isLoggedIn={isLoggedIn}
                      onLogin={this.onLogin} />
                  )} />
                <Route path="/secret"
                  render={() => (
                    <SecretPage isLoggedIn={isLoggedIn} />
                  )} />
                <Route render={() => <h2>Page not found!</h2>} />
              </Switch>

            </div>
          </Router>
        </SwapiServiceProvider>
      </ErrorBoundry>
    );
  }
}

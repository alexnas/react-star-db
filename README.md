This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# React StarDB

This React application shows various information about Star Wars heroes, planets and spaceships.

## Uses

- React
- React Router
- Context
- PropTypes

## Installation and usage

- Be sure that Git and Node.js are installed globally.
- Clone this repo.
- Run `npm install`, all required components will be installed automatically.
- Run `npm start` to start the project.
- Run `npm build` to create a build directory with a production build of the app.
- Run `npm test` to test the app.

## License

Under the terms of the MIT license.
